<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['verified']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


    Route::get('community', [App\Http\Controllers\CommunityLinkController::class, 'index']);

    Route::post('community', [App\Http\Controllers\CommunityLinkController::class, 'store']);

    Route::get('community/{channel}', [App\Http\Controllers\CommunityLinkController::class, 'index']);

    Route::post('votes/{communityLink}', [App\Http\Controllers\CommunityLinkUserController::class, 'store']);

    Route::post('upload-image', [App\Http\Controllers\UserAvatarController::class, 'store']);

    Route::get('upload-image', [App\Http\Controllers\UserAvatarController::class, 'index']);
});