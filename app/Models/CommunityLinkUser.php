<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommunityLinkUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'community_link_id',
        'user_id',
    ];

    public function toggle(){
        if($this->exists){
            $this->delete();
        }else{
            $this->save();
        }
    }
}
