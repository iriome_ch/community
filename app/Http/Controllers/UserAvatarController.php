<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserAvatarController extends Controller
{

    public function index()
    {
        return view('avatar/user-avatar');
    }


    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $request->file('image')->store('public/avatars');
        
        Auth::user()->image = asset('storage/avatars/' . $request->file('image')->hashName());
        Auth::user()->save();

        return redirect()->route('home')->with('status', 'Avatar actualizado');
    }
}
