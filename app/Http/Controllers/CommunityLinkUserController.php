<?php

namespace App\Http\Controllers;

use App\Models\CommunityLink;
use App\Models\CommunityLinkUser;
use Illuminate\Http\Request;

class CommunityLinkUserController extends Controller
{
    public function store(CommunityLink $communityLink)
    {
        // Busca si el usuario ya ha votado y si no lo ha votado aún crea un nuevo registro
        $vote = CommunityLinkUser::firstOrNew([
            'community_link_id' => $communityLink->id,
            'user_id' => auth()->id(),
        ])->toggle();
        // Si el id del voto existe lo borra y si no lo guarda.
        // if($vote->id) {
        //     $vote->delete();
        // }else{
        //     $vote->save();
        // }
        // Redirecciona a la página del enlace
        return back();
    }
}
