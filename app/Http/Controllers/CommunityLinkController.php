<?php

namespace App\Http\Controllers;

use App\Models\Channel;
use App\Models\CommunityLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CommunityLinkForm;
use App\Queries\CommunityLinksQuery;

class CommunityLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Channel $channel = null)
    {
        //El Route Model Binding nos permite obtener el objeto Channel que se corresponde con el atributo que se le pasa en la ruta, por defecto es el id del objeto Channel pero le podemos pasar un nombre de atributo diferente para que se corresponda con el atributo que se le pasa en la ruta. Esto se hace con el metodo getRouteKeyName() de la clase Model.
        //dd($channel);
        if(request()->exists('search')&& request()->exists('popular')){
            $links = (new CommunityLinksQuery)->searchWithPopular(request()->search);
        } else if(request()->exists('search')){
            $links = (new CommunityLinksQuery)->search(trim(request()->get('search')));
        }else if (request()->exists('popular')) {
            $links = (new CommunityLinksQuery())->popular();
        }else {
            if($channel)$links = (new CommunityLinksQuery())->channel($channel);
            else $links = (new CommunityLinksQuery())->all();
        }

        $channels = Channel::orderBy('title', 'asc')->get();
        return view('community/index', compact('links', 'channels', 'channel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommunityLinkForm $request)
    {
        // Retorna el objeto de la petición
        //dd($request);
        // Retorna el array de datos del formulario
        //dd($request->all());
        // Retorna la ruta sin el dominio de la llamada
        //dd($request->path());
        // Retorna la ruta con el dominio de la llamada
        //dd($request->url());
        //dd($request->fullUrl());
        // Retorna el array de todos los datos de la petición
        //dd($request->input());
        $approved = Auth::user()->trusted ? 1 : 0;
        $request->merge(['user_id' => auth()->user()->id, 'approved' => $approved]);

        $link = new CommunityLink();
        $link->user_id = Auth::id();
        if ($approved == 1) {
            if ($link->hasAlreadyBeenSubmitted($request->link)) {
                return back()->with('success', 'El enlace ya había sido enviado, se ha actualizado la fecha de subida.');
            }
            CommunityLink::create($request->all());
            return back()->with('success', 'Link creado correctamente y aprobado');
        } else {
            if ($link->hasAlreadyBeenSubmitted($request->link)) {
                return back()->with('success', 'El enlace ya había sido enviado, se ha actualizado la fecha de subida.El enlace será revisado por un administrador.');
            }
          
            CommunityLink::create($request->all());
            return back()->with('success', 'Link creado correctamente. Debe ser aprobado por un administrador');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function show(CommunityLink $communityLink)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function edit(CommunityLink $communityLink)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommunityLink $communityLink)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommunityLink $communityLink)
    {
        //
    }
}
