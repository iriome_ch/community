<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class sendlink extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendMail:link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mail to user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $details = [
            'title' => 'Mail from Community',
            'body' => 'This is the body of the mail',
        ];
        
        \Mail::to('iriomech@gmail.com')->send(new \App\Mail\CommunityMail($details));
    }
}
