<?php

namespace App\Queries;

use App\Models\Channel;
use App\Models\CommunityLink;
use Illuminate\Http\Request;


class CommunityLinksQuery
{
    public function popular()
    {
        $links = CommunityLink::withCount('users')->orderBy('users_count', 'desc')->with('channel')->where('approved', 1)->paginate(25);
        return $links;
    }

    public function search($search)
    {
        $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY); 

        $links = CommunityLink::where(function($query) use ($searchValues) {
           foreach($searchValues as  $value) {
               if(!empty($value)) {
                   $query->orWhere('title' , 'like', '%'.$value.'%');
               }
           }
        })->where('approved', 1)->with('channel')->paginate(25);
        return $links;
    }

    public function channel($channel)
    {
        $links = $channel->communitylink()->orderBy('updated_at', 'desc')->where('approved', 1)->paginate(25);
        return $links;
    }

    public function all()
    {
        $links = CommunityLink::orderBy('updated_at', 'desc')->where('approved', 1)->paginate(25);
        return $links;
    }

    public function searchWithPopular($search)
    {
        $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY); 

        $links = CommunityLink::where(function($query) use ($searchValues) {
           foreach($searchValues as  $value) {
               if(!empty($value)) {
                   $query->orWhere('title' , 'like', '%'.$value.'%');
               }
           }
        })->where('approved', 1)->with('channel')->withCount('users')->orderBy('users_count', 'desc')->paginate(25);
        return $links;
    }
}
