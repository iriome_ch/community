<div class="col-md-4">
    <div class="card">
        <div class="card-header">
            <h3>Contribute a link</h3>
        </div>
        <div class="card-body">
            <!-- En el controlador se llama al método store -->
            <form method="POST" action="/community">
                <!-- Añade le CSRF Token, que es una clave que se usa para proteger las peticiones de formularios de ataques CSRF -->
                @csrf
                <div class="form-group">
                    <label for="title">Title:</label>

                    <input type="text" class="form-control" id="title" name="title" placeholder="What is the title of your article?" value="{{old('title')}}">
                    @error('title')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="link">Link:</label>
                    <input type="text" class="form-control" id="link" name="link" placeholder="What is the URL?" value="{{old('link')}}">
                    @error('link')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="Channel">Channel:</label>
                    <select class="form-control @error('channel_id') is-invalid @enderror" name="channel_id">
                        <option selected disabled>Pick a Channel...</option>
                        @foreach ($channels as $channel)
                        <option value="{{ $channel->id }}" {{ old('channel_id') == $channel->id ? 'selected' : '' }}>{{ $channel->title }}</option>
                        @endforeach
                    </select>
                    @error('channel_id')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group card-footer">
                    <button class="btn btn-primary">Contribute Link</button>
                </div>
            </form>
        </div>
    </div>

</div>