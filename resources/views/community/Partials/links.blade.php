@foreach ($links as $link)
<div class="card mb-3">
    <div class="card-header">
        <a href="{{ $link->link }}">{{ $link->title }}</a>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <p>{{ $link->link }}</p>
            </div>
            <div class="col-md-2">
                @if ($link->channel->color == 'red')
                <a href="/community/{{ $link->channel->slug }}" class="badge bg-danger text-decoration-none">{{ $link->channel->title }}</a>
                @elseif ($link->channel->color == 'blue')
                <a href="/community/{{ $link->channel->slug }}" class="badge bg-primary text-decoration-none">{{ $link->channel->title }}</a>
                @elseif ($link->channel->color == 'green')
                <a href="/community/{{ $link->channel->slug }}" class="badge bg-success text-decoration-none">{{ $link->channel->title }}</a>
                @elseif ($link->channel->color == 'yellow')
                <a href="/community/{{ $link->channel->slug }}" class="badge bg-warning text-decoration-none">{{ $link->channel->title }}</a>
                @elseif ($link->channel->color == 'orange')
                <a href="/community/{{ $link->channel->slug }}" class="badge bg-secondary text-decoration-none">{{ $link->channel->title }}</a>
                @endif
            </div>
            <div class="col-md-2">
                <form method="POST" action="/votes/{{ $link->id }}">
                    @csrf
                    <button type="submit" class="btn btn-sm {{ Auth::check() && Auth::user()->votedFor($link) ? 'text-success' : 'text-secondary' }}" {{ Auth::guest() ? 'disabled' : '' }}>
                        
                        <i data-feather="thumbs-up"></i>
                    </button>
                    {{ $link->users()->count() }}
                </form>
            </div> 
        </div>
    </div>
    <div class="card-footer">
        <small class="text-muted">Contributed by: {{$link->creator->name}} {{$link->updated_at->diffForHumans()}}</small>
    </div>
</div>
@endforeach