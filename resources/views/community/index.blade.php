@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <h1>
                <a href="/community" class="text-decoration-none text-dark">
                    Community/
                </a>

                @if ($channel && $channel->color == 'red')
                <span class="text-danger">{{ $channel->title }}</span>
                @elseif ($channel && $channel->color == 'blue')
                <span class="text-primary">{{ $channel->title }}</span>
                @elseif ($channel && $channel->color == 'green')
                <span class="text-success">{{ $channel->title }}</span>
                @elseif ($channel && $channel->color == 'yellow')
                <span class="text-warning">{{ $channel->title }}</span>
                @elseif ($channel && $channel->color == 'orange')
                <span class="text-secondary">{{ $channel->title }}</span>
                @else
                <span class="text-dark">All</span>
                @endif
            </h1>
            <hr>

            <a href="/community" class="btn btn-sm btn-secondary px-1">Most Recent</a>
            @if (request()->exists('search'))
            <a href="?popular&&search={{ request()->search }}" class="btn btn-sm btn-secondary px-1">Most Popular</a>
            @else
            <a href="?popular" class="btn btn-sm btn-secondary px-1">Most Popular</a>
            @endif
            @include('community.Partials.links')
            @if (count($links) == 0)
            <p>No contributions have been made yet.</p>
            @endif

        </div>
        @include('community.partials.add-link')
    </div>
    {{ $links->appends($_GET)->links() }}

</div>


@stop