<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Community.test</title>
    </head>
    <body>
        <h1>{{ $details['title'] }}</h1>
        <p>{{ $details['body'] }}</p>

        <p>Thank you</p>
    </body>